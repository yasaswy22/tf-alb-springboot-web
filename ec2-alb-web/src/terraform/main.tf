terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.63.0"
    }
  }
}

provider "aws" {
  access_key = var.tf_access_key
  secret_key = var.tf_secret_key
  region     = var.tf_region
}

module "vpc" {
  source                  = "./modules/vpc"
  tf_vpc_cidr_block       = "10.0.0.0/16"
  tf_subnet_public_cidrs  = ["10.0.1.0/24", "10.0.2.0/24"]
  tf_subnet_private_cidrs = ["10.0.3.0/24", "10.0.4.0/24"]
  
  tf_vpc_sg_name = var.tf_ec2_security_group
  tf_vpc_tag_name = var.tf_tag_name

}

module "sgs" {
  source = "./modules/sgs"
  tf_ssh_sg = var.tf_sg_ssh_name
  tf_http_sg = var.tf_sg_http_name
  tf_tomcat_sg = var.tf_sg_tomcat_name
  tf_vpc = "${module.vpc.tf_vpc_id}"
  tf_tag_name = var.tf_tag_name
}

module "alb" {
  source = "./modules/alb"
  tf_alb_tg = var.tf_alb_target_group_name
  tf_vpc = "${module.vpc.tf_vpc_id}"
  tf_ec2_1 = "${module.ec2.tf_ec2_1_id}"
  tf_ec2_2 = "${module.ec2.tf_ec2_2_id}"
  tf_alb = var.tf_alb_name
  tf_security_groups = ["${module.sgs.tf_ssh_sg_id}",
                        "${module.sgs.tf_http_sg_id}",
                        "${module.sgs.tf_tomcat_sg_id}"]
  tf_subnets = "${module.vpc.tf_public_subnets}"
  tf_alb_tag_name = var.tf_tag_name
}

module "ec2" {
  source = "./modules/ec2"
  tf_ami_id = var.tf_ami_id
  tf_instance_type = var.tf_instance_type
  tf_security_groups = ["${module.sgs.tf_ssh_sg_id}",
                        "${module.sgs.tf_http_sg_id}",
                        "${module.sgs.tf_tomcat_sg_id}"]

  tf_subnets = "${module.vpc.tf_public_subnets}"
  tf_iam_instance_profile = "${module.policies.tf_ec2_instance_profile}"

  tf_ec2_tag_name = var.tf_tag_name
}

module "s3" {
  source = "./modules/s3"

  tf_bucket_name = var.tf_s3_bucket
  tf_tag_name = var.tf_tag_name
}

module "policies" {
  source = "./modules/policies"

  tf_ec2_instance_profile_name = var.tf_ec2_instance_profile
  tf_ec2_access_role_policy_name = var.tf_ec2_access_role_policy
  tf_ec2_s3_access_role_name = var.tf_ec2_s3_access_role
}

module "iam" {
  source = "./modules/iam"
  
  tf_user_names = var.tf_user_names
  tf_user_group_name = var.tf_user_group_name
  tf_user_group_assignment_name = var.tf_user_group_assignment_name
  tf_user_policy_name = var.tf_user_policy_name
  tf_user_policy_arn = var.tf_user_policy_arn
}