# Commands to run when ec2 launches
yum install java-1.8.0-openjdk -y #-y flag automatically confirms any prompts.
# alternatives --config java - command if there is already a version installed in ec2 by default and to change it to jre-1.8.0

# https://tf-custom-s3-bucket01.s3.amazonaws.com/springboot-alb-web.jar far jar with springboot mvn project uploaded to s3
# make sure the above jar in s3 has public access
wget https://tf-custom-s3-bucket01.s3.amazonaws.com/springboot-alb-web.jar # to download the jar to ec2
java -jar springboot-alb-web.jar # to run the jar file

# to access it use https://ec2-54-209-190-166.compute-1.amazonaws.com:8080/