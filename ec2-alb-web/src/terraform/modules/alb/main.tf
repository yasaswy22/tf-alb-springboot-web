# unhealthy_treshold: if it has bad response to the curl command
# to the ec2 instance 2 timesin the below example
# it decides as unhealthy instance, will stop routing to it.
# healthy_treshold: to start routing to it again it will 
# hverify the curl command response 5 times in the below example
# after the success it will start routing it to that instance again.

# to create target groups, it can be instance or ip or lambda
# each type has its own syntax
resource "aws_lb_target_group" "this" {
  health_check {
    interval = 10
    path = "/"
    protocol = "HTTP"
    timeout = 5
    healthy_threshold = 5
    unhealthy_threshold = 2
  }

  name = var.tf_alb_tg
  port = 80
  protocol = "HTTP"
  target_type = "instance"
  vpc_id = var.tf_vpc
}

// to attach alb to instances
resource "aws_lb_target_group_attachment" "tf_alb_ec2_1" {
    target_group_arn = "${aws_lb_target_group.this.arn}"
    target_id = var.tf_ec2_1
    port = 80
}

resource "aws_lb_target_group_attachment" "tf_alb_ec2_2" {
    target_group_arn = "${aws_lb_target_group.this.arn}"
    target_id = var.tf_ec2_2
    port = 80
}

// to create the actual lb
resource "aws_lb" "this" {
  name = var.tf_alb
  internal = false

  security_groups = var.tf_security_groups
  
  subnets = var.tf_subnets
  ip_address_type = "ipv4"
  load_balancer_type = "application"

  tags = {
    "Name" = "alb_${var.tf_alb_tag_name}"
  }
}

#  which port and protocol this lb is going to listen
resource "aws_lb_listener" "this" {
  load_balancer_arn = "${aws_lb.this.arn}"
  port = 80
  protocol = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.this.arn}"
  }
}

