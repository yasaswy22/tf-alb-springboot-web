output "tf_ec2_1_id" {
  value = "${element(aws_instance.this.*.id, 1)}"
}

output "tf_ec2_2_id" {
  value = "${element(aws_instance.this.*.id, 2)}"
}