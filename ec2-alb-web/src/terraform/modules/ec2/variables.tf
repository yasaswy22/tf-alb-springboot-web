variable "tf_ec2_tag_name" {}

variable "tf_subnets" {
  type = list(string)
}

variable "tf_ami_id" {}

variable "tf_instance_type" {}

variable "tf_iam_instance_profile" {
  type = string
}

variable "tf_security_groups" {
  type = list(string)
}