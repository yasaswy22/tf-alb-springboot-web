# Get the list of availability zones
data "aws_availability_zones" "this" {}

# Create ec2 instance
resource "aws_instance" "this" {
  count = "${length(var.tf_subnets)}"
  instance_type          = "${var.tf_instance_type}"
  ami                    = var.tf_ami_id
  iam_instance_profile   = var.tf_iam_instance_profile
  vpc_security_group_ids = var.tf_security_groups
  subnet_id              = "${element(var.tf_subnets, count.index)}"


  tags = {
    "Name" = "ec2_${count.index}_${var.tf_ec2_tag_name}"
  }
}

# To give user data when ec2 is created
data "template_file" "user_data"{
  template = "${file("${path.module}/userdata.tpl")}"
}