variable "tf_user_names" {
  type = list(string)
}

variable "tf_user_group_name" {
  
}

variable "tf_user_policy_arn" {
  
}

variable "tf_user_group_assignment_name" {
  
}

variable "tf_user_policy_name" {
  
}