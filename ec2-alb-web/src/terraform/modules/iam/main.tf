#Creating users

# Definition of IAM Users and groups
resource "aws_iam_user" "this" {

  # The count meta-argument can be used to create a resource as many times as you define, in this case length of usernames list.
  # Using count.index after the name will give a distinct index number corresponding to each iteration of the resource block.

  count = length(var.tf_user_names)
  name  = element(var.tf_user_names, count.index)
}

resource "aws_iam_group" "this" {
  name = var.tf_user_group_name
}

# Assign the users to group
resource "aws_iam_group_membership" "this" {

  name  = var.tf_user_group_assignment_name
  users = var.tf_user_names
  group = aws_iam_group.this.name
}

# Attach policy to the group
resource "aws_iam_policy_attachment" "this" {
  name   = var.tf_user_policy_name
  groups = [aws_iam_group.this.name]

  # The permission policy requires you to specify the resoource using ARN format
  # arn:partition:service:region:account:resource
  policy_arn = var.tf_user_policy_arn
}