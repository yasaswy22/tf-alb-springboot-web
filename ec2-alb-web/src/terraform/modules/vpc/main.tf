# Usecase:
# . Create vpc
# . Create route table
# . Create Network ACL (NACL) Router (Firewall) subnet level
# . Create Security Group Instance/VM/Physical Machine (Firewall/iptables)
# -----------------
# . Create subnet
# . Create Custome (public) route table
# . Create Internet gateway
# . Use port 22
# . Use Tenancy default and dedicated

# VPC in US east: 10.0.0.0/16
# availability zone status:
# us-west-2a: 10.0.1.0/24 public subnet, 10.0.2.0/24 public subnet
# us-west-2b: 10.0.3.0/24 private subnet, 10.0.4.0/24 private subnet
# us-west-2c:
# us-west-2d:


# Get the list of availability zones
data "aws_availability_zones" "this" {}

# Create a vpc
resource "aws_vpc" "this" {
  cidr_block = var.tf_vpc_cidr_block
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
      Name = "vpc_${var.tf_vpc_tag_name}"
  }
}

# Create an internet gateway
resource "aws_internet_gateway" "this" {
  vpc_id = aws_vpc.this.id

  tags = {
	  "Name" = "ig_${var.tf_vpc_tag_name}"
	}
}

# Create a public route table
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.this.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.this.id
  }

  tags = {
	  "Name" = "public_rt_${var.tf_vpc_tag_name}"
	}
}

# Create a default route table
resource "aws_default_route_table" "private_rt" {
  default_route_table_id = aws_vpc.this.default_route_table_id

  tags = {
	  "Name" = "private_rt_${var.tf_vpc_tag_name}"
	}
}

# Create a public subnet
resource "aws_subnet" "public_subnet" {
  count = "${length(var.tf_subnet_public_cidrs)}"
  cidr_block = "${element(var.tf_subnet_public_cidrs, count.index)}"
  map_public_ip_on_launch = true
  vpc_id = aws_vpc.this.id
  availability_zone = "${element(data.aws_availability_zones.this.names, count.index)}"

  tags = {
	  "Name" = "public_snet_${var.tf_vpc_tag_name}"
	}
}

# Create a private subnet
resource "aws_subnet" "private_subnet" {
  count = "${length(var.tf_subnet_private_cidrs)}"
  cidr_block = "${element(var.tf_subnet_private_cidrs, count.index)}"
  vpc_id = aws_vpc.this.id
  availability_zone = "${element(data.aws_availability_zones.this.names, count.index)}"

  tags = {
	  "Name" = "private_snet_${var.tf_vpc_tag_name}"
	}
}

resource "aws_route_table_association" "public_subnet_rt_assoc" {
  count = "${length(aws_subnet.public_subnet)}"
  subnet_id = "${element(aws_subnet.public_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.public_rt.id}"
}

resource "aws_route_table_association" "private_subnet_rt_assoc" {
  count = "${length(aws_subnet.private_subnet)}"
  subnet_id = "${element(aws_subnet.private_subnet.*.id, count.index)}"
  route_table_id = "${aws_default_route_table.private_rt.id}"
}