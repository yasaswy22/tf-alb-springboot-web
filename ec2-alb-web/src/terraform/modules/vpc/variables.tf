variable "tf_vpc_cidr_block" {
        type = string
}

variable "tf_subnet_public_cidrs" {
        type = list(string)
}

variable "tf_subnet_private_cidrs" {
        type = list(string)
}

variable "tf_vpc_tag_name" {
  type = string
}

variable "tf_vpc_sg_name" {
  type = string
}