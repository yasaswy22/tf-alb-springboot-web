output "tf_public_subnets" {
  value = aws_subnet.public_subnet.*.id
}

output "tf_vpc_id" {
  value = aws_vpc.this.id
}