resource "aws_security_group" "tf_ssh_sg" {
  name        = var.tf_ssh_sg
  vpc_id      = var.tf_vpc
  description = "Allow ssh inbound traffic, outbound all traffic"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name" = "${var.tf_ssh_sg}_${var.tf_tag_name}"
  }
}

resource "aws_security_group" "tf_http_sg" {
  name        = var.tf_http_sg
  vpc_id      = var.tf_vpc
  description = "Allow http & https inbound traffic, outbound all traffic"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name" = "${var.tf_http_sg}_${var.tf_tag_name}"
  }
}

resource "aws_security_group" "tf_tomcat_sg" {
  name        = var.tf_tomcat_sg
  vpc_id      = var.tf_vpc
  description = "Allow tomcat port 8080 inbound traffic, outbound all traffic"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name" = "${var.tf_tomcat_sg}_${var.tf_tag_name}"
  }
}