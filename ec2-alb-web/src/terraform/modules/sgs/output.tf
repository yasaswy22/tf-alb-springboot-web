output "tf_ssh_sg_id" {
 value = aws_security_group.tf_ssh_sg.id
}

output "tf_http_sg_id" {
 value = aws_security_group.tf_http_sg.id
}


output "tf_tomcat_sg_id" {
 value = aws_security_group.tf_tomcat_sg.id
}
