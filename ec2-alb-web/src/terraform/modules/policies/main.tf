
# Use case: 
# . Create a S3 bucket
# . Create a custom role
# . Create a custom policy
# . Role should able to get access to the S3 bucket
# . Attach the policy to the S3 bucket, for allowing request from an ec2 instance
# . Associate the role with this ec2 instance,
#     so we'll be able to verify if ec2 instance is able to access S3 bucket 
#     without any authorization

# Create role to access the AWS S3 bucket
resource "aws_iam_role" "res01" {
  name = var.tf_ec2_s3_access_role_name
  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  # Principal service will be an ec2 instance here.
  # Role mentions it gets access to ec2 instance.
  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "Ec2AssumingARole"
        "Action" : "sts:AssumeRole",
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "ec2.amazonaws.com"
        },
      }
    ]
  })
}

# Create policy to atach the S3 bucket role
# Below IAM policy defines what the above principal "ec2.amazonaws.com" can do 
# in our AWS environment.
resource "aws_iam_role_policy" "res02" {
  name = var.tf_ec2_access_role_policy_name
  role = aws_iam_role.res01.name
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : "s3:*",
        "Resource" : [
          "*"
          # "arn:aws:s3:::${var.tf_s3_bucket}",  // authorize s3 bucket instance, getting bucket name from var
          # "arn:aws:s3:::${var.tf_s3_bucket}/*" // authorize contents inside the s3 bucket instance
        ]
      }
    ]
  })
}

# Instance identifier

# An instance profile is a container for an IAM role that you can use 
# to pass role information to an EC2 instance when the instance starts.
# This allows any application running on the instance 
# to access certain resources defined in the role policies. 
# Instance profiles are usually recommended over configuring a static access key 
# as they are considered more secure and easier to maintain.
resource "aws_iam_instance_profile" "res03" {
  name = var.tf_ec2_instance_profile_name
  role = aws_iam_role.res01.name
}
