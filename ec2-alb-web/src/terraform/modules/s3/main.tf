# Create s3 bucket
resource "aws_s3_bucket" "this" {
  bucket = var.tf_bucket_name

  # acl stands for access control list
  # Amazon S3 access control lists (ACLs) enable you to manage access to buckets and objects.

  # acl = "private"
  tags = {
    "Name" = var.tf_tag_name
  }
}