
variable "tf_region" {
  type = string
}

variable "tf_access_key" {
  type = string
}

variable "tf_secret_key" {
  type = string
}

variable "tf_tag_name" {
  type = string
  default = "tf_practice"
}

variable "tf_ami_id" {
  type = string
}

variable "tf_instance_type" {
  type = string
}

variable "tf_user_names" {
  type = list(string)
  default = ["tf_Aguy", "tf_Bguy"]
}

variable "tf_user_group_name" {
  type = string
  default = "tf_ec2_create_allow"
}

variable "tf_user_group_assignment_name" {
  type = string
  default = "tf_ec2_creators"
}

variable "tf_user_policy_name" {
  type = string
  default = "tf_user_ec2_full_access"
}

variable "tf_user_policy_arn" {
  type = string
  default = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}

variable "tf_s3_bucket" {
  type = string
  default = "tf-custom-s3-bucket01"
}

variable "tf_ec2_s3_access_role" {
  type = string
  default = "tf_ec2_s3_access_role"
}

variable "tf_ec2_access_role_policy" {
  type = string
  default = "tf_custom-s3-bucket-role-policy"
}

variable "tf_ec2_instance_profile" {
  type = string
  default = "tf_s3-access-instance-profile"
}

variable "tf_ec2_security_group" {
  type = string
  default = "tf_ec2_sg"
  }

  variable "tf_alb_target_group_name" {
    type = string
    default = "tf-ec2-alb-tg"
  }

  variable "tf_alb_name" {
    type = string
    default = "tf-ec2-alb"
  }

  variable "tf_sg_ssh_name" {
    type = string
    default = "tf_sg_ssh"
  }

  variable "tf_sg_http_name" {
    type = string
    default = "tf_sg_http"
  }

  variable "tf_sg_tomcat_name" {
    type = string
    default = "tf_sg_tomcat"
  }