package tf.web.ec2albweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ec2AlbWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ec2AlbWebApplication.class, args);
	}
}
