package tf.web.ec2albweb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller /* This is not a REST controller that returns json responses, instead we want a html response */
public class PortfolioController {

	@GetMapping("/")
    public String indexPage(){
        return "index";
    }

    @GetMapping("/index.html")
    public String indexNavPage(){
        return "index";
    }

    @GetMapping("/about.html")
    public String aboutPage(){
        return "about";
    }
}
