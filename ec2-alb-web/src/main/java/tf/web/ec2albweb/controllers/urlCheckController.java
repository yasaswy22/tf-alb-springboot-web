package tf.web.ec2albweb.controllers;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class urlCheckController {

    private final String SITE_IT_UP = "Site is up";
    private final String SITE_IT_DOWN = "Site is down";
    private final String INCORRECT_URL = "Urlis incorrect";

    @GetMapping("/check")
    public String getUrlStatusMessage(@RequestParam String url) {
        String returnMessage = "";
        try {
            URL urlObj = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            int responseCodeCategory = conn.getResponseCode() / 100;
            if (responseCodeCategory == 2 || responseCodeCategory == 3) {
                returnMessage = SITE_IT_UP;
            } else {
                returnMessage = SITE_IT_DOWN;
            }
        } catch (MalformedURLException e) {
            returnMessage = INCORRECT_URL;
        } catch (IOException e) {
            returnMessage = SITE_IT_DOWN;
        }

        return returnMessage;
    }
}

// To Test:
// Try something like http://localhost:8080/check?url=https://www.google.com/ in browser